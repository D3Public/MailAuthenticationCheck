<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

namespace D3\MailAuthenticationCheck;

use D3\MailAuthenticationCheck\Mechanism\DMARC\RejectPolicy;
use D3\MailAuthenticationCheck\Model\DMARCRecord as Record;
use D3\MailAuthenticationCheck\Model\DMARCResult as Result;
use Mika56\SPFCheck\DNS\DNSRecordGetterInterface;
use Mika56\SPFCheck\Exception\DNSLookupException;
use Mika56\SPFCheck\Model\Query;

class DMARCCheck
{
    protected DNSRecordGetterInterface $DNSRecordGetter;

    public function __construct(DNSRecordGetterInterface $DNSRecordGetter)
    {
        $this->DNSRecordGetter = $DNSRecordGetter;
    }

    /**
     * @param string $domainName
     * @return Record[]
     * @throws DNSLookupException
     */
    public function getDomainDMARCRecords(string $domainName): array
    {
        $result = [];

        if (!str_starts_with($domainName, '_dmarc.')) {
            $domainName = '_dmarc.'.$domainName;
        }

        $records = $this->DNSRecordGetter->resolveTXT($domainName);

        foreach ($records as $record) {
            $txt = strtolower($record);
            // An DMARC record can be empty (default policy)
            if ($txt == 'v=dmarc1;' || str_starts_with($txt, 'v=dmarc1; ')) {
                $result[] = new Record($record);
            }
        }

        return $result;
    }

    public function getResult(Query $query): Result
    {
        return $this->doGetResult($query);
    }

    private function doGetResult(Query $query): Result
    {
        $domainName = $query->getDomainName();

        $result??= new Result();

        if(empty($domainName)) {
            $result->setResult(Result::NONE);
            return $result;
        }

        try {
            $records = $this->getDomainDMARCRecords( $domainName);
        } catch (DNSLookupException $e) {
            $result->setResult(Result::TEMPERROR);

            return $result;
        }

        if (count($records) == 0) {
            $result->setResult(Result::NONE);

            return $result;
        }
        if (count($records) > 1) {
            $result->setResult(Result::PERMERROR);

            return $result;
        }

        $record = $records[0];
        $result->setRecord($record);
        if (!$record->isValid()) {
            $result->setResult(Result::PERMERROR);

            return $result;
        }

        foreach ($record->getTerms() as $term) {
            if ( $term instanceof RejectPolicy) {
                $result->setResult($term->getValue());
            }
        }

        return $result;
    }
}
<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

namespace D3\MailAuthenticationCheck\Mechanism\DMARC;

use D3\MailAuthenticationCheck\Mechanism\AbstractMechanism;
use D3\MailAuthenticationCheck\Model\DMARCResult;

class RejectPolicy extends AbstractMechanism
{
    public function getValue(): string
    {
        switch (strtolower((string) $this)) {
            case    'quarantine':
                return DMARCResult::REJECT_QUARANTINE;
            case    'reject':
                return DMARCResult::REJECT_REJECT;
            case    'none':
            default:
                return DMARCResult::REJECT_NONE;
        }
    }
}

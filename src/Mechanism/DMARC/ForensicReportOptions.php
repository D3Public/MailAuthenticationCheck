<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

namespace D3\MailAuthenticationCheck\Mechanism\DMARC;

use D3\MailAuthenticationCheck\Mechanism\AbstractMechanism;

class ForensicReportOptions extends AbstractMechanism
{
    public const DEFAULT = '0';

    public function getList(): array
    {
        return explode(':', trim(strtolower((string) $this)));
    }

    public function SpfAndDkimAlignmentFailed(): bool
    {
        return in_array('0', $this->getList());
    }

    public function SpfOrDkimAlignmentFailed(): bool
    {
        return in_array('1', $this->getList());
    }

    public function DkimFailed(): bool
    {
        return in_array('d', $this->getList());
    }

    public function SpfFailed(): bool
    {
        return in_array('s', $this->getList());
    }
}

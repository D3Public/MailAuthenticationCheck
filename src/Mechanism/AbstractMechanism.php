<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

namespace D3\MailAuthenticationCheck\Mechanism;

use Mika56\SPFCheck\Mechanism\AbstractMechanism as SpfAbstractMechanism;

abstract class AbstractMechanism extends SpfAbstractMechanism
{
    private string $qualifier;
    private string $content;

    /**
     * @param string $rawTerm
     * @param string $qualifier
     * @param string $termContent
     */
    public function __construct(string $rawTerm, string $qualifier, string $termContent)
    {
        parent::__construct($rawTerm, $qualifier);
        $this->qualifier = $qualifier;
        $this->content = $termContent;
    }

    /**
     * @return string
     */
    public function getQualifier(): string
    {
        return $this->qualifier;
    }

    public function __toString(): string
    {
        return $this->content;
    }
}

<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

namespace D3\MailAuthenticationCheck\Model;

use D3\MailAuthenticationCheck\Model\DMARCRecord as Record;

class DMARCResult
{
    public const REJECT_NONE = 'none';
    public const REJECT_QUARANTINE = 'quarantine';
    public const REJECT_REJECT = 'reject';

    public const NONE = 'None';
    public const TEMPERROR = 'TempError';
    public const PERMERROR = 'PermError';

    private string $result;

    protected Record $record;

    public function hasResult(): bool
    {
        return isset($this->result);
    }

    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @internal
     */
    public function setRecord(Record $record): self
    {
        $this->record = $record;

        return $this;
    }

    public function getRecord(): Record
    {
        return $this->record ?? new Record('');
    }

    /**
     * @internal
     */
    public function setResult(string $result): self
    {
        $this->result      = $result;
        return $this;
    }
}

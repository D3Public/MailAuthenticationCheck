<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

namespace D3\MailAuthenticationCheck\Enum;

abstract class DMARCMechanism
{
    public const REJECT_POLICY = 'p';
    public const SUB_REJECT_POLICY = 'sp';
    public const REPORT_URI_AGGREGATE = 'rua';
    public const REPORT_URI_FORENSIC = 'ruf';
    public const SPF_ALIGNMENT = 'aspf';
    public const DKIM_ALIGNMENT = 'adkim';
    public const REPORTING_INTERVAL = 'ri';
    public const REPORTING_FORMAT = 'rf';
    public const FORENSIC_REPORT_OPTIONS = 'fo';
    public const PERCENTAGE = 'pct';
}
